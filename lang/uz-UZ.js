export default {
  company:'Kompaniya',
  name_surname:'Ism va familiya',
  new_link:'Kajduyu ssilku na novoy strok',
  new_problem:'Siz qanday muammoni hal qilishimizni xohlaysiz?',
  "navlink1": "Biz haqimizda",
  "navlink2": "Xizmatlar",
  "navlink3": "Portfolio",
  "navlink4": "Jamoa",
  "navlink5": "Blog",
  "home": "Mijozlarimizning hayotini osonlashtirish",
  "company_description": "Biz raqamli marketing bo'yicha 10 yillik tajribaga ega mutaxassislar tomonidan asos solingan marketing agentligimiz. ",
  "more": "Batafsil",
  "address": "г.Ташкент, ул. Зульфияханум 12 Alpha Business Center",
  "why_us": "Nega biz?",
  "why_us_text": "Biz raqamli marketing sohasida yetakchi va pionerlardan birimiz. Portfoliomizda “TBC Bank”, “Nescafe”, “Pepsi”, “Samarqand Darvoza”, “Makro”, “Central Park” va boshqa 100 dan ortiq mahalliy va xalqaro kompaniyalar mavjud.",

  all: 'Hammasi',
  smm: 'SMM',
  advert_compony:'Reklama kampaniyalari',
  influence_marketing:'Marketingga ta`sir o`tkazish',
  brendings:'Brendlash',

  "succed_projects": "Muvaffaqiyatli loyihalar",
  "clients": "SMM sohasidagi mijozlar",
  "logo_and_branding": "yillik tajriba",
  "sites": "ortiq xodimlar",
  "we_do_unique_tasks": "Biz noyob ishni qilamiz",
  "our_partners": "Bizning sheriklarimiz",
  "call_back": "Arizangizni yuboring",
  "not_all": "Bu hali hammasi emas",
  "all_projects": "Barcha loyihalar",
  "all_services": "Barcha xizmatlar",
  "team_description": "Bizning jamoamiz marketingning turli sohalari bo'yicha mutaxassislardan iborat.\n" +
    "Biz bozorlar va odamlarni tadqiq qilamiz, strategiyalar ishlab chiqamiz, brendlar yaratamiz va reklama kampaniyalarini olib boramiz.",
  "interesting?": "Biz ajoyib loyihalar uchun arizalarni qabul qilamiz. Iltimos, qisqacha shaklni to'ldiring va biz siz bilan bog'lanamiz!",
  "contacts_button": "Контакты",
  contact_us: 'Arizangizni yuboring',
  whereLocate: 'Bizning manzilimiz',
  "portfolio_text1": "Biz raqamli marketing sohasida yetakchi va pionerlardan birimiz. Portfoliomizda “TBC Bank”, “Nescafe”, “Pepsi”, “Samarqand Darvoza”, “Makro”, “Central Park” va boshqa 100 dan ortiq mahalliy va xalqaro kompaniyalar mavjud.",
  "portfolio_text2": "Biz mijozlarimizga aniq biznes maqsadlariga erishishga yordam beramiz va mavjud resurslar doirasida eng samarali yechimlarni taqdim etamiz.\n" +
    "Biz muayyan bozor turining holatini o'rganamiz, raqobatchilar va maqsadli auditoriyani tahlil qilamiz va koʻzlangan maqsadga olib boruvchi strategiyani taklif qilamiz.",
  "SMM": "МАРКЕТИНГ В СОЦИАЛЬНЫХ СЕТЯХ",
  "text_services1": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "ad_companies": "РАЗРАБОТКА РЕКЛАМНЫХ КОМПАНИИЙ",
  "text_services2": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "marketing_research": "МАРКЕТИНГОВЫЕ ИССЛЕДОВАНИЯ",
  "text_services3": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "branding_and_design": "БРЕНДИНГ И ДИЗАЙН",
  "text_services4": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "influince_marketing": "INFLUENCE MARKETING",
  "text_services5": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "production": "PRODUCTION",
  "text_services6": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "media_service": "МЕДИА ОБСЛУЖИВАНИЕ",
  "text_services7": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "coordination_of_work": "КООРДИНАЦИЯ РАБОТЫ МАРКЕТИНГА НА АУТСОРС",
  "text_services8": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",


  "task": "Задача",

  "solution": "Решение",

  "results": "Результаты",
  "increase1": "Amet ipsum dolor",
  "increase2": "Lorem ipsum dolor sit amet, consectetur",
  "increase3": "Lorem ipsum dolor sit amet, consectetur",
  "increase_description": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы.",


  "theme": "Ushbu maqolada uni qanday izlash kerak va u bilan nima qilish kerak",
  "theme_description": "Ushbu oddiy va arzon vositalarni aylantiring, o'qing va foydalaning :)",
  theme2:'2021 yilda chiroyli portlashmi? ☄️',
  theme_description2:'Bu yilgi dizayn tendentsiyalari uchun cheat sahifasini saqlang, geometrik 3D shakllar trendda davom etmoqda va o\'z pozitsiyalaridan voz kechmoqchi emas',

  theme3:'Ushbu hafta oxiri ko\'radigan hech narsa yo\'qmi? Ajoyib filmlar va seriallarni tomosha qiling 👇🏻 ',
  theme_description3:'',


  "blog_theme": "FROM CONCRETE TO <br> ABSTRACT",
  "blog_text1": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br><br> We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  "blog_text2": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <br><br>We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",

  "email": "Электронная почта",
  "location": "Адрес",
  "address_contacts": "ул. Зульфияханум 12 <br> Alpha Business Center",
  "tel": "Телефон",
  "name_of_your_company": "Название Вашей компании",
  "site_of_company": "Адрес сайта компании",
  "name_and_surname": "Ваше имя и фамилия",
  "socilals_of_company": "Ссылки на социальные сети компании",
  "contact_tel": "Контактный телефон",
  "purpose": "Ваша цель обращения в агентство",
  "send": "Отправить",
  "alls": "увидеть всё",
  article1:' Siz odatda ko\'zingizni uza olmaydigan reklama posti yoki nashrni ko\'rib, oʻzingizga - "Jin ursin, bu juda oson, juda ajoyib, lekin nega buni o\'zim qilmadim?"-degan savolni berasizmi?\n' +
    '            <br><br>\n' +
    '            Chunki kimdir yaxshi izlanishlar olib borgan va maqsadga to\'g\'ri eltuvchi insayt tanlashgan 🔎\n' +
    '            <br><br>\n' +
    '            Aynan mana shu ma\'lumotlar ishingizni foydali va samarali qiladi\n' +
    '            <br><br>\n' +
    '            Ushbu maqolada uni qanday izlash va u bilan nima qilish kerakligi keltirilgan\n' +
    '            <br><br>\n' +
    '            Ushbu oddiy va arzon uskunalarni koʻring, o\'qing va foydalaning :)',
  article2:'         2021-yilga go\'zal qadam bosish? ☄️\n' +
    '            <br><br>\n' +
    '            Bu yilgi dizayn trendi uchun muhim omillarni eslab qoling\n' +
    '            <br><br>\n' +
    '            1. Sokin ranglar va lakonik dizayn. Loyihalaringizning qadriyatlarini aks ettiruvchi ranglarni tanlang\n' +
    '            <br><br>\n' +
    '            2. Murakkablashtirmang. Yoqimli ko\'rinish uchun flet ikonkalar, sodda shakllar\n' +
    '            <br><br>\n' +
    '            3. Kertik harflar yana urfda. Nostalji va nafislik\n' +
    '            <br><br>\n' +
    '            4. Geometrik 3D shakllar trendi davom etmoqda va o\'z o\'rnini hali hech kimga bermoqchi emas\n' +
    '            <br><br>\n' +
    '            5. Tabiiy bo\'yoqlar va tabiiy shakllar — 2021-yilda ko\'zlar uchun rohat\n',
  article3:'Dam olish kunlari ko\'rishga hech narsa yo\'qmi? Ajoyib filmlar va seriallarni tomosha qiling 👇🏻\n' +
    '            <br><br>\n' +
    '            “Aqldan ozganlar” (2010) teleseriali\n' +
    '            <br><br>\n' +
    '            Har qanday marketolog uchun #Mustwatch serial. Bu serial  toʻgʻri slogan hamma narsani sotishi mumkin boʻlgan 1960-yillardagi  reklama beruvchilar olamiga shoʻngʻishga undaydi. Ish joyidagi antiqa voqealar, mijozlarning o\'zaro munosabatlari va ijodiy jarayon faqat bitta savol qoldiradi: "Qaysi kasb marketologdan yaxshiroq bo\'lishi mumkin?"\n' +
    '            <br><br>\n' +
    '            “Emili Parijda” (2020) teleseriali\n' +
    '            <br><br>\n' +
    '            Bir qarashda kino yuzaki va real hayotdagi reklamadan yiroq ko\'rinishi mumkin. Biroq, siz Emilidan ko\'p narsalarni o\'rganishingiz mumkin, masalan, risk va sinovlardan qo\'rqmaslik, oʻz auditoriyangiz ovoziga quloq solish va tevarak-atrofingizdan ilhom izlash.\n' +
    '            <br><br>\n' +
    '            “Bu yerda chekiladi” (2005, AQSh) filmi\n' +
    '            <br><br>\n' +
    '            Odamlarni oʻziga ergashtirish uchun to\'g\'ri so\'zlarni topa bilish va ishontirish qobiliyati - bosh qahramon Nilning eng kuchli xususiyatidir. Film ishontirish san\'ati bilan jamiyat va uning asoslariga qanday qarshi turishni oʻrgatadi.\n' +
    '            <br><br>\n' +
    '            “Skandal” (2012, AQSh) teleseriali\n' +
    '            <br><br>\n' +
    '            Sevgi yoʻllari, drama, hamma narsani boy berish va obro\'ga putr yetkazishdan qo\'rqish. Olivia va uning jamoasi, shuningdek, PR (mashhur) odamlari foydalanadigan qiziqarli yondashuvlar va vositalar haqida hikoya.\n' +
    '            <br><br>\n' +
    '            “Qartalar uyi” (2013 yil, AQSh) teleseriali\n' +
    '            <br><br>\n' +
    '            Vahshiylik, xiyonat va janjallarga to\'la serial. Kongress a\'zosi Frank Andervud saylovlar poygasida oʻzi yordam bergan AQShning yangi prezidentidan oʻch oladi. Diqqat bilan Frankning harakatlarini kuzatib boring, ularda ko\'plab professional insaytlarni topishingiz mumkin.',




  marketing: {
    main: "Ijtimoiy tarmoqlarda marketing",
    1: "Ijtimoiy tarmoqlar strategiyasi",
    2: "Maqsadli reklama",
    3: "Kommyuniti-menejment",
    4: "Kommyuniti-menejment",
    5: "Internetda brendni qayd etish monitoringi",
    6: "Kontent yaratish",
    7: "Media rejalashtirish",
    8: "Tahlil",
  },


  coordination: {
    main: "Marketing ishlarini autsors tarzda muvofiqlashtirish",
    1: "Marketing bo'limida ish jarayonlarini yo'lga qo'yish",
    2: "Marketing bo'limida ish jarayonlarini yo'lga qo'yish",
    3: "Ishchilar uchun ko'rsatmalar",
    4: "Ishchilar uchun ko'rsatmalar",
    5: "Jamoa bilan birgalikda mashg'ulotlar va seminarlar",
    6: "Bo'lim ishini optimallashtirish uchun turli vositalarni tatbiq etish",
  },


  integration: {
    main: "Integratsiyalashgan reklama kampaniyalari",
    1: "Integratsiyalashgan reklama kampaniyalari",
    2: "Integratsiyalashgan reklama kampaniyalari",
    3: "Reklama kampaniyasi rejasini ishlab chiqish",
    4: "Byudjetlashtirish",
    5: "Media rejalashtirish",
    6: "Amalga oshirish",
  },

  research: {
    main: "Marketing tadqiqotlari",
    1: "Bozor tahlili va turi: holati va rivojlanish istiqbollari",
    2: "Maqsadli auditoriya: segmentlar va iste'molchilar haqidagi tushunchalar",
    3: "Raqobatchilar: asosiy o'yinchilar, ularning afzalliklari va kamchiliklari",
    4: "Mukammal suhbatlar, miqdoriy tadqiqotlar o'tkazish  va fokus-guruhlarni aniqlash",

  },


  strategy: {
    main: "Joylashuv strategiyasini ishlab chiqish",
    1: "Bozorni o'rganish va tahlil qilish",
    2: "Iste'molchilar va raqobatchilar tahlili",
    3: "tovar konsepsiyasi, maqsad va vazifalarini ishlab chiqish",
    4: "Matnli shior va nomlash",
    5: "Tovarning ijtimoiy platformasi (ijtimoiy xususiyati, asosiy xabar, bozorda noyob brend taklifi)",
  },
  brending: {
    main: "Brending va dizayn",
    1: "Logotip va firma simvolikasining boshqa elementlarini ishlab chiqish;",
    2: "O‘ziga xos firma stili va brendbukni ishlab chiqish;",
    3: "Vizual aloqa (ranglar, shriftlar va boshqa elementlar)",
    4: "Xizmat dizayni",
    5: "2D va 3D animatsiya",
    6: "Motion dizayn",
    7: "AR niqoblarini yaratish",
    8: "Telegram uchun stikerlar yaratish",
  },
  webSite: {
    main: "Veb sayt yaratish va uni targʻib qilish",
    1: "Diagnostika va audit",
    2: "Veb-sayt tuzulishini yaratish",
    3: "Veb sahifalar dizayni",
    4: "Tahlil tizimini ulash",
    5: " Gʻoya yetakchilari bilan ishlash",
    6: "Maxsus loyihalar va promo-aksiya",
    7: "Profil tahlili",
  },

  Production: {
    main: "Production",
    1: "Video ishlab chiqarish:",
    2: "Intervyu",
    3: "Sahna dekoratsiyasi",
    4: "Sahnalashtirilgan (aktyorlar ishtirokida)",
    5: "  Informatsion",
    6: " Animatsion",
    7: "  Tijoriy / reklamaga oid",
    8: "  Fotomahsulot",
  },

  media: {
    main: "Media xizmati",
    1: " Istirohat bog'lari va savdo ko'ngilochar markazlaridagi reklama vositalariga joylashtirish",
    2: "Ommaviy axborot vositalari va blogerlarda reklama joylashtirish",
    3: "Media rejalashtirish",
  },


  alisher: {
    name: "Alisher Jumaniyozov",
    position: "Taʼsischi",
  },
  anton: {
    name: "Anton Chexov",
    position: "Taʼsischi",
  },

  anvar: {
    name: "Anvar Abdullayev",
    position: "Grafik dizayner",
  },
  denis: {
    name: "Denis Roman",
    position: "Direktor va taʼsischi",
  },
  diana: {
    name: "Diana Suleymanova",
    position: "Grafik dizayner",
  },
  elbek: {
    name: "Elbek Nurmuhamedov",
    position: "Grafik dizayner",
  },
  elyora: {
    name: " Elyora Atabayeva",
    position: "SMM menejeri",
  },
  feruz: {
    name: "Feruzxon Yoqubxoʻjayev",
    position: "Taʼsischi",
  },
  finat: {
    name: "Finat Rizyapov ",
    position: "Fotograf",
  },
  guzalB: {
    name: "Goʻzal Bayliyeva ",
    position: "SMM menejeri",
  },
  guzalT: {
    name: "Goʻzal Toʻxtasinova ",
    position: "SMM menejeri",
  },
  malika: {
    name: "Malika Qambarova",
    position: "Boʻlim boshligʻi",
  },
  mirafzal: {
    name: "Mirafzal Mirahmadov",
    position: " Kopirayter",
  },
  bonu: {
    name: "Muhtarambonu Xonboboyeva ",
    position: "Muhtarambonu Xonboboyeva",
  },
  oybek: {
    name: "Oybek Karimov",
    position: "Bosh dizayner",
  },
  oybekT: {
    name: "Oybek Toʻxtayev",
    position: "Tadqiqot ishlari boʻyicha mutaxassis",
  },
  sanjarS: {
    name: "Sanjar Shoyusupov",
    position: " Haydovchi ",
  },
  sherzodA: {
    name: "Sherzod Ahmedov",
    position: " Loyiha menejeri ",
  },
  shirinF: {
    name: "Shirin Fayziyeva ",
    position: " Kopirayter  ",
  },
  shohiddinS: {
    name: "Shoxiddin Sarimsoqov",
    position: " Ijrochi menejer",
  },
  timurS: {
    name: "Timur Silyayev",
    position: " Loyiha menejeri, tadqiqot boʻlimi boshligʻi  ",
  },
  umidS: {
    name: "Umid Saparbayev",
    position: "Grafik dizayner",
  },
  valeriyaL: {
    name: "Valeriya Li ",
    position: "Loyiha menejeri",
  },

  nikolay: {
    name: "Nikolay ",
    position: "Проект менеджер ",
  },
  alexSelivanova: {
    name: "Alexandra Selivanova",
    position: "",
  },

  david: {
    name: "David",
    position: "",
  },

  malikaZayni: {
    name: "Malika Zayniyeva",
    position: "",
  },

  tamila: {
    name: "Tamila",
    position: "",
  },

  TemurNew: {
    name: "Temur",
    position: "",
  },
  Vasiliya: {
    name: "Vasiliya",
    position: "",
  },
  "task_description": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  "solution_description": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  "results_description": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы.",

  "task_descriptionSMM": "- Повысить узнаваемость бренда полумарафона <br>  - Повысить число зарегистрированных участников на сайте и поддержать продажу фирменных сувениров <br> - Собрать необходимую сумму для благотворительной цели полумарафона",
  "solution_descriptionSMM": "- Подключили к освещению мероприятия лидеров мнений, которые рассказали о полумарафоне и открыли собственные клубы для участия <br> - Разработали предложения для корпоративного сегмента, подключив к участию несколько крупных компаний <br>- Разработали и запустили AR-маски в Instagram для вовлечения аудитории <br> + - Настроили и запустили таргетированную рекламу для разных групп участников на весь мир <br>  - Сотрудничали с мобильными операторами и подключили СМС-рассылки",
  "results_descriptionSMM": "- 3.000 зарегистрированных участников <br>  - 35 стран участников полумарафона <br> - 15.000 посетителей сайта <br> - Собрана необходимая сумма для создания художественной студии в одном из интернатов Узбекистана для детей с аутизмом",


  "task_descriptionAdv": "- Разработать концепцию и айдентику фестиваля <br>- Увеличить количество посетителей <br>- Привлечь спонсоров для сотрудничества",
  "solution_descriptionAdv": "- Разработали визуальную айдентику фестиваля <br> - Провели масштабную рекламную кампанию  (диджитал, наружная реклама, радио, СМИ, блогеры и т.д)<br> - Разработали концепции Tik Tok зон для привлечения молодой аудитории <br> - Подготовили и подарили гифтбоксы с печеньками и персональными предсказаниями среди блогеров и лидеров мнений <br> - Разработали и запустили AR-маску в Instagram с веселыми предсказаниями на 2021 год для вовлечения аудитории\n",
  "results_descriptionAdv": "- Общий медиа охват составил более 14 000 000 контактов <br> - 50 блогеров и лидеров мнений получили гифтбоксы фирменными подарками, персональным приглашением и предсказанием на 2021 год",


  "task_descriptionAdv2": "- Вывести бренд нового киберпространства на рынок Узбекистана",
  "solution_descriptionAdv2": "- Разработали и реализовали рекламную кампанию по выводу бренда на рынок <br> - Провели креативную инфлюенс кампанию с дропами от Cyber Arena, как в популярной игре PUBG <br> - Создали сотни единиц контента для социальных сетей (Instagram, Facebook, YouTube, Tik Tok, Twitch, Telegram)<br> - Совместно с брендом разработали и реализовали концепцию ивента открытия <br>- Разработали AR-маску, где пользователи могли почувствовать себя героями из популярной игры Dota 2",
  "results_descriptionAdv2": "- Более 1 700 000 охваченных пользователей в социальных сетях <br>  - 13 блогеров получили гифтбоксы в виде дропов из популярной игры PUBG перед открытием Арены <br> - Более 50 запросов было получено на покупку дропов <br> - 4 000 пользователей подписались на профиль Cyber Arena в Instagram ",


  "task_descriptionInf": "- Повысить узнаваемость агентства за счет креативной инфлюенс кампании\n" +
    "- Поздравить клиентов,  партнеров, лидеров мнений и друзей агентства с наступающим годом",
  "solution_descriptionInf": "Контекст: \n" +
    "\n" +
    "2020 год был сложным и нервным периодом для многих. \n" +
    "Для спокойствия и крепких нервов в новом 2021 году мы решили подготовить антистрессовый набор подарков от We Digital.\n" +
    "\n" +
    "Что было внутри?\n" +
    "Пузырчатая упаковка. Здесь все понятно. Все и так знают ее успокаивающие свойства\n" +
    "Массажер — паучок для головы. Использовать в новом году можно после чтения очередных новостей из новостной ленты\n" +
    "Две бутылочки виски. Рекомендуются к приему во время составления годовых отчетов\n" +
    "Антистрессовая игрушка и раскраска. Использовать по нервным понедельникам\n" +
    "Карандаши для раскраски, которые в отличие от ручек не щелкают, чтобы никого не раздражать\n" +
    "Две пары носков с обозначениями, чтобы не запутаться и не нервничать по этому поводу с надписями: “Левый”, “Правый” и два “Запасных”\n" +
    "Также под крышкой коробки был QR код, который вел на канал с успокаивающими видео",
  "results_descriptionInf": "- Подарили 100 гифтбоксов клиентам, партнерам и лидерам мнений, с которыми мы работаем <br> - Блогеры с суммарным количеством подписчиков более 1 300 000 пользователей упоминули агентство в социальных сетях <br> - Получили десятки запросов на покупку копий боксов в директ <br> - Миллионы нервных клеток будут сохранены в 2021 ",


  "task_descriptionInf2": "- Провести PR-кампанию по освещению трансляции презентации",
  "solution_descriptionInf2": "• Реализовали медиаразмещение на авторских и других крупных Telegram каналах и онлайн медиа ресурсах Узбекистана <br> • Подключили к освещению ивента узбекских инфлюенсеров. <br> • Осуществили вместе с командой видеопродакшна техническое сопровождение трансляции в прямом эфире в Instagram ",
  "results_descriptionInf2": "4.6k + подписчиков в Instagram аккаунте MEDIAPARK <br> 169k + охваченных пользователей в Instagram посредством размещений у инфлюенсеров <br> 785k + охваченных пользователей в Telegram <br>1.8k + составил пик просмотров во время эфира в Instagram",

  "task_descriptionInf3": "- Рассказать о новом продукте Nescafe Arabica <br> - Привлечь инфлюенсеров Узбекистана ",
  "solution_descriptionInf3": "-  Подарили 50 бифтбоксов с персональными креативными надписями на кружках 50 блогерам\n",
  "results_descriptionInf3": "- Более 400 000 охваченных пользователей <br> - 98% упоминаний бренда от блогеров",

  "task_descriptionBrend": "- Создать бренд страховой организации в Узбекистане",
  "solution_descriptionBrend": "- Разработали нейминг <br> - Создали айдентику бренда (логотип, фирменный стиль и гайдбук) <br> - Забрендировали более 50 различных носителей (визитки, календари, канцелярские принадлежности и т.д)<br> - Разработали визуальную коммуникацию в социальных сетях\n",


  "task_descriptionBrend2": "- Создать бренд нового языкового центра в Узбекистане",
  "solution_descriptionBrend2": "- Разработали нейминг <br> - Создали айдентику бренда (логотип, фирменный стиль и гайдбук) <br> - Разработали дизайн веб-сайта <br>- Брендинг носителей <br>- Приняли участие в создании интерьера и экстерьера языкового центра",

  "task_descriptionBrend3": "- Создать бренд новой сети строительных материалов по Узбекистану",
  "solution_descriptionBrend3": "- Разработали нейминг <br>- Создали айдентику бренда (логотип, фирменный стиль и гайдбук) <br>- Брендинг носителей, фирменных коробок и униформы персонала",


}


