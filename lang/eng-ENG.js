export default {
  company:'Company',
  name_surname:'First and last name',
  new_link:'Each link on a new line',
  new_problem:'What problem do you want us to solve?',
  "navlink1": "About us",
  "navlink2": "Services",
  "navlink3": "Portfolio",
  "navlink4": "Team",
  "navlink5": "Blog",
  "home": "Making our clients' lives easier",
  "company_description": "We are a marketing agency founded by specialists with 10 years of experience in the field of digital marketing. ",
  "more": "In detail",
  "address": "г.Ташкент, ул. Зульфияханум 12 Alpha Business Center",
  "why_us": "Why us?",
  "why_us_text": "\n" +
    "We help our clients achieve specific business objectives and provide the most effective solutions within the available resources.\n" +
    "We investigate the state of a particular market category, analyze competitors, target audience, and propose a strategy that will lead to the goal.",

  all: 'All',
  smm: 'SMM',
  advert_compony:'Advertising campaigns',
  influence_marketing:'Influence marketing',
  brendings:'Branding',

  "succed_projects": "Успешных проектов",
  "clients": "Клиентов в сфере SMM",
  "logo_and_branding": "years of experience",
  "sites": "Сайты и порталы",
  "we_do_unique_tasks": "Мы делаем уникальные работы",
  "our_partners": "Our partners",

  "call_back": "Submit your application",
  "not_all": "That's not all",
  "all_projects": "all projects",
  "all_services": "Все услуги",
  "team_description": "Our team consists of specialists in different areas of marketing. We research markets and people, develop strategies, create brands and run advertising campaigns.",
  "interesting?": "We accept applications for cool projects. Please fill out the brief form and we will contact you!",
  "contacts_button": "Контакты",
  contact_us: 'Submit your application',
  whereLocate: 'Our address',
  "portfolio_text1": "We are one of the leaders and pioneers in the digital environment. Our portfolio includes brands such as TBC Bank, Nescafe, Pepsi, Samarqand Darvoza, Makro, Central Park and over 100 other local and international companies.",
  "portfolio_text2": "We help our clients achieve specific business objectives and provide the most effective solutions within the available resources. We research the state of a separate market category, analyze competitors and target audience, and propose a strategy that will lead to the set goal.",
  "SMM": "SOCIAL MEDIA MARKETING",
  /////////
  "text_services1": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "ad_companies": "РАЗРАБОТКА РЕКЛАМНЫХ КОМПАНИИЙ",
  "text_services2": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "marketing_research": "МАРКЕТИНГОВЫЕ ИССЛЕДОВАНИЯ",
  "text_services3": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "branding_and_design": "БРЕНДИНГ И ДИЗАЙН",
  "text_services4": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "influince_marketing": "INFLUENCE MARKETING",
  "text_services5": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "production": "PRODUCTION",
  "text_services6": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "media_service": "МЕДИА ОБСЛУЖИВАНИЕ",
  "text_services7": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",
  "coordination_of_work": "КООРДИНАЦИЯ РАБОТЫ МАРКЕТИНГА НА АУТСОРС",
  "text_services8": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Нам важно, с кем мы работаем, важно, какие у всех нас цели и принципы. Мы не просто делаем рекламу, мы создаем идею, которая отвечает на потребности бизнеса и становится частью жизни потребителей.",


  "task": "Task",

  "solution": "Decision",

  "results": "Results",
  "increase1": "Amet ipsum dolor",
  "increase2": "Lorem ipsum dolor sit amet, consectetur",
  "increase3": "Lorem ipsum dolor sit amet, consectetur",
  "increase_description": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы.",


  "theme": "How to look for it and what to do with it in this article",
  "theme_description": "Scroll, read and use these simple and affordable tools :) Read about how to find it and what to do with it in this article",

  theme2:'Bursting beautifully in 2021?☄️',
  theme_description2:'Keep a cheat sheet on design trends for this year. Natural shades and shapes a pleasure for the eyes in 2021',

  theme3:'Nothing to see this weekend? Catch a selection of cool movies and TV series 👇🏻',
  theme_description3:'',


  "blog_theme": "FROM CONCRETE TO <br> ABSTRACT",
  "blog_text1": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br><br> We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  "blog_text2": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <br><br>We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",

  "email": "Email",
  "location": "Address",
  "address_contacts": "ул. Зульфияханум 12 <br> Alpha Business Center",
  "tel": "Телефон",
  "name_of_your_company": "Your company name",
  "site_of_company": "Company website address",
  "name_and_surname": "Your name and surname",
  "socilals_of_company": "Links to social networks of the company",
  "contact_tel": "Contact number",
  "purpose": "Your purpose of contacting the agency",
  "send": "Send message",
  "alls": "see everything",
  article1: 'How often do you see a promotional post or post that catches you and you wonder: "Damn, it\'s so simple, but so cool, why didn\'t I do it myself?"\n <br>Because they\'ve done good research and caught insights that hit the target exactly 🔎 <br>This is exactly the kind of info that will make your work useful and effective <br>Read about how to find it and what to do with it in this article <br>Scroll through, read, and use these simple and accessible tools :)',
  article2:' Break in beautifully in 2021  <br> Keep a cheat sheet on design trends for this year <br> 1. Calm tones and concise design. Choose colors that reflect the values of your projects <br>2. Don\'t complicate things. Flat icons, simplified shapes for a pleasant visual experience <br>3. Serif fonts are gaining popularity again. Nostalgia and elegance <br>4. Geometric 3D shapes continue to be in the trend and are not going to give up their positions <br>5. Natural shades and shapes a pleasure for the eyes in 2021',
  article3:' Nothing to see on the weekend? Catch a set of cool movies and TV series 👇  🏻 <br>Mad Men (2010) TV series <br>The #Mustwatch series is for any marketer. It plunges into the inner kitchen of the 60s advertisers, when the right slogan could sell everything. Crazy situations in the workplace, interaction with customers and the creative process, will leave only one question: "What profession other than as a marketer can be better?» <br>Emily in Paris (2020) TV series <br>At first glance, the series may seem superficial and far from advertising in real life. However, you can learn a lot from Emily, for example, to take risks and not be afraid of experiments, to listen to the voice of your audience, to look for inspiration around you <br>Thank You for Smoking (2005, USA) film <br>The ability to persuade and choose the right words to inspire people to follow you is the strongest feature of the main character Neil. The film is about how you can resist society and its foundations with the art of persuasion. <br>Scandal (2012, USA) TV series <br>The love line, the drama, the fear of losing everything and ruining your reputation. The story is about a cool PR woman named Olivia and her team, as well as interesting approaches and tools that PR people use. <br>House of Cards (2013, USA) TV series <br>A series full of cynicism, betrayal and scandals. Congressman Frank Underwood is taking revenge on the new US president, whom he helped in the election race. Take a closer look and observe the actions of Frank, you can find a lot of professional insights in them',
  marketing: {
    main: "Social Media Marketing",
    1: "Social media presence strategy",
    2: "Targeted advertising",
    3: "Community Management",
    4: "Working with media resources",
    5: "Monitoring brand mentions on the Internet",
    6: "Content creation",
    7: "Media Planning",
    8: "Analytics",
  },


  coordination: {
    main: "Coordination of outsourced marketing",
    1: "Building the processes of work in the marketing department",
    2: "Setting tasks for the department and monitoring their implementation",
    3: "Instructions for employees",
    4: "Formation and differentiation of areas of responsibility",
    5: "Collective trainings with the team and workshops",
    6: "Implementation of tools to optimize the work of the department\n" +
      "(task management and monitoring, promotion, reporting)\n",
  },


  integration: {
    main: "Integrated advertising campaigns",
    1: "Development of the advertising campaign concept",
    2: "Determination the main channels of communication with the target audience",
    3: "Development of an advertising campaign plan",
    4: "Budgeting",
    5: " Media planning",
    6: "Implementation",
  },

  research: {
    main: "Marketing research  ",
    1: "Market and category analysis: state and prospects of development",
    2: "Target audience: segments and consumer insights",
    3: "Competitors: the main players, their advantages and disadvantages",
    4: "Conducting in-depth interviews, quantitative research,\n" +
      "and focus groups",

  },


  strategy: {
    main: "Developing a positioning strategy",
    1: "Market research and study",
    2: "Analysis of consumers and competitors",
    3: "Development of the brand concept, goals and objectives",
    4: " Text tagline and naming",
    5: "Brand communication platform (the nature of communication, the main message, the brand's unique offer in the market)",
  },
  brending: {
    main: "Branding and design",
    1: "Development of the logo and other elements of corporate symbols;",
    2: "Development of corporate identity and brand book;",
    3: "Visual communication (colors, fonts, and other elements)",
    4: "Service design",
    5: "2D and 3D animation",
    6: "Motion design",
    7: "AR masks creation",
    8: "Telegram stickers creation",

  },
  webSite: {
    main: "Website development and promotion",
    1: "Diagnostics and audit",
    2: " Creation of site structure",
    3: "Web page design",
    4: "Connecting analytics systems",
    5: " Influence marketing",
    6: " Working with opinion leaders",
    7: " Profile Analytics",
  },

  Production: {
    main: "Production ",
    1: "Video Production:",
    2: "Interviews",
    3: "Backstage",
    4: "Staged (with the participation of actors)",
    5: " Informational",
    6: " Animated features",
    7: "  Commercial / Promotional",

  },

  media: {
    main: "Media Service",
    1: "Placement on advertising media in parks and malls",
    2: "Placement in the media and with bloggers",
    3: "Media Planning",
  },


  alisher: {
    name: "Alisher Djumaniyazov",
    position: "Co-founder",
  },
  anton: {
    name: "Anton Chekhov",
    position: "Co-founder",
  },

  anvar: {
    name: "Anvar Abdullaev",
    position: "Graphic Designer",
  },
  denis: {
    name: "Denis Roman",
    position: "CEO & Co-founder",
  },
  diana: {
    name: "Diana Suleymanova",
    position: "Graphic Designer ",
  },
  elbek: {
    name: "Elbek Nurmukhamedov",
    position: "Graphic Designer ",
  },
  elyora: {
    name: " Elyora Atabaeva",
    position: "SMM manager",
  },
  feruz: {
    name: "Feruzkhan Yakoubkhodjaev",
    position: "Co-founder",
  },
  finat: {
    name: "Razyapov Finat ",
    position: "Photographer",
  },
  guzalB: {
    name: "Guzal Baylieva ",
    position: "SMM manager",
  },
  guzalT: {
    name: "Guzal Tukhtasinova ",
    position: "SMM manager",
  },
  malika: {
    name: "Malika Kambarova ",
    position: "Head of department",
  },
  mirafzal: {
    name: "Мирафзал Мирахмадов ",
    position: " Copywriter",
  },
  bonu: {
    name: "Мухтарамбону Ханбабаева ",
    position: " Account manager ",
  },
  oybek: {
    name: "Oybek Karimov",
    position: " Senior designer ",
  },
  oybekT: {
    name: "Oybek Tukhtaev",
    position: " Research Specialist",
  },
  sanjarS: {
    name: "Sanjar Shayusupov",
    position: " Driver ",
  },
  sherzodA: {
    name: "Sherzod Ahmedov",
    position: " Project Manager ",
  },
  shirinF: {
    name: "Shirin Fayzieva ",
    position: " Copywriter ",
  },
  shohiddinS: {
    name: "Shohiddin Salimsakov ",
    position: " Executive manager ",
  },
  timurS: {
    name: "Timur Selyaev ",
    position: " Project Manager, Head of Research Department  ",
  },
  umidS: {
    name: "Umid Saparbaev",
    position: "Graphic Designer  ",
  },
  valeriyaL: {
    name: "Valeria Lee ",
    position: "Project Manager  ",
  },

  nikolay: {
    name: "Nikolay ",
    position: "Проект менеджер ",
  },
  alexSelivanova: {
    name: "Alexandra Selivanova",
    position: "",
  },

  david: {
    name: "David",
    position: "",
  },

  malikaZayni: {
    name: "Malika Zayniyeva",
    position: "",
  },

  tamila: {
    name: "Tamila",
    position: "",
  },

  TemurNew: {
    name: "Temur",
    position: "",
  },
  Vasiliya: {
    name: "Vasiliya",
    position: "",
  },
  "task_description": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  "solution_description": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  "results_description": "We.Digital - это агентство, состоящее из специалистов в сфере диджитал маркетинга с 10-летним опытом работы.",

  "task_descriptionSMM": "- Increase brand awareness of the half marathon br <br> - Increase the number of registered participants on the site and support sales of branded souvenirs <br>- Raise the necessary amount for the charity purpose of the half marathon",
  "solution_descriptionSMM": "- We attracted opinion leaders to cover the event, who told about the half marathon and opened their own clubs for participation <br> - Developed proposals for the corporate segment, involving several large companies - Developed and launched AR masks on Instagram to engage the audiencez <br> - Set up and run targeted ads for different groups of participants around the world <br> - We cooperated with mobile operators and enabled SMS Bulk Delivery",
  "results_descriptionSMM": "- 3,000 registered participants <br> - 35 countries participating in the half marathon <br> - 15,000 site visitors <br>- The required amount has been collected to create an art studio in one of the boarding schools in Uzbekistan for children with autism",


  "task_descriptionAdv": "- Develop the concept and visual identity of the festival <br> - Increase the number of visitors <br> - Attract sponsors for cooperation",
  "solution_descriptionAdv": "- Developed the visual identity of the festival <br> - Conducted a large-scale advertising campaign (digital, outdoor advertising, radio, media, bloggers, etc.) <br> - Developed the concept of Tik Tok zones to attract a young audience <br> - Prepared and presented giftboxes with cookies and personal predictions among bloggers and opinion leaders <br> - Developed and launched an AR mask on Instagram with hilarious predictions for 2021 to engage the audience",
  "results_descriptionAdv": "- Total media coverage was more than 14,000,000 contacts <br> - 50 bloggers and opinion leaders received giftboxes with branded gifts, a personal invitation and a prediction for 2021",


  "task_descriptionAdv2": "- To bring the brand of the new cyberspace to the market of Uzbekistan",
  "solution_descriptionAdv2": "- Developed and implemented an advertising campaign to bring the brand to the market <br> - Conducted a creative influencer campaign with CyberArena drops, just like in the popular PUBG game <br> Instagram Facebook, YouTube, Tik Tok, Twitch, Telegram-We have created hundreds of pieces of content for social networks.) <br> -  In collaboration with the brand, we developed and implemented the concept of the opening event <br>- Developed an AR mask where users could feel like heroes from the popular Dota 2 game",
  "results_descriptionAdv2": "- More than 1,700,000 social media users reached <br>- 13 bloggers received giftboxes in the form of drops from the popular game PUBG before the opening of the Arena <br> - More than 50 requests were received for the purchase of giftboxes <br>- 4,000 users have subscribed to the Cyber Arena profile on Instagram",


  "task_descriptionInf": "- Increase the agency awareness through the creative influence campaign <br> - To congratulate the agency's clients, partners, opinion leaders and friends on the upcoming year",
  "solution_descriptionInf": "Context: The year 2020 was a difficult and nervous period for many. For calm and strong nerves in the new year 2021, we decided to prepare an anti-stress set of gifts from WeDigital. What was inside? Bubble wrap. Everything is clear here. Everyone already knows its calming properties <br> Head massage device. You can use it in the new year after reading the next news from the news feed <br>Two bottles of whiskey. It is recommended to use it during the preparation of annual reports <br> Anti-stress toy and coloring book. Use on nervous Mondays <br> Coloring pencils, which, unlike pens, do not click, so as not to annoy anyone <br> Two pairs of socks with symbols, so as not to get confused and not to get nervous about it with the inscriptions: \"Left”,\" Right” and two \" Spare” <br> Also under the lid of the box was a QR code that led to a channel with soothing videos",
  "results_descriptionInf": "-  We gave 100 gift boxes to our clients, partners and opinion leaders with whom we work <br>-  Bloggers with a total number of subscribers of more than 1,300,000 users mentioned the agency in social networks <br>- Received dozens of requests to purchase copies of boxes in direct messages <br> - Millions of nerve cells will be saved in 2021",

  first_translate:'The first broadcast of the Apple presentation from MEDIAPARK in Uzbekistan',
  "task_descriptionInf2": "- Conduct a PR campaign to cover the broadcast of the presentation",
  "solution_descriptionInf2": "• We have implemented media placement on copyright and other major Telegram channels and online media resources in Uzbekistan <br>• Involved Uzbek influencers to cover the event. <br> • Together with the video production team, we provided technical support for the live broadcast on Instagram",
  "results_descriptionInf2": "4.6 + followers on MEDIAPARK's Instagram account <br> 169k + reached users on Instagram through ad placement at influencers' accounts <br> 785k + covered users in Telegram <br>1.8 k + made up the peak of views during the broadcast on Instagram",

  "task_descriptionInf3": "- Announce the new Nescafe Arabica product <br> - Attract Uzbek influencers",
  "solution_descriptionInf3": "-  Подарили 50 бифтбоксов с персональными креативными надписями на кружках 50 блогерам\n",
  "results_descriptionInf3": "- Более 400 000 охваченных пользователей <br> - 98% упоминаний бренда от блогеров",

  "task_descriptionBrend": "- Create a brand of an insurance company in Uzbekistan",
  "solution_descriptionBrend": "- Developed nami <br>- Created the brand identity (logo, corporate identity and guidebook) <br>- We have branded more than 50 different products and handouts (business cards, calendars, stationery, etc.).) <br>- Developed visual communication in social networks",
  results_descriptionBrend3:'',

  "task_descriptionBrend2": "- Create a brand of a new language center in Uzbekistan",
  "solution_descriptionBrend2": "- Developed naming <br>- Created the brand identity (logo, corporate identity and guidebook) <br>- Developed a website design <br>- Porduct branding <br>- Participated in the creation of the interior and exterior of the language center",

  "task_descriptionBrend3": "- Create a brand of a new chain of stores selling construction materials in Uzbekistan",
  "solution_descriptionBrend3": "- Developed naming  <br>- Created the brand identity (logo, corporate style, guidebook) <br>- Product branding, branded boxes, and staff uniforms",


}


