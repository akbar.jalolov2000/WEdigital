


export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'WEdigital',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '@/node_modules/bootstrap/dist/css/bootstrap.min.css',
    "~/assets/style/slick",

  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [

    { src: '~/plugins/ymapPlugin.js',  mode: 'client' },
    // '@plugins/v-mask.js',
    {src: '@/plugins/v-mask.js'},
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)


  modules: [
    [
      'nuxt-i18n',
      {
        locales: [
          {
            code: 'ru',
            file: 'ru-RU.js'
          },
          {
            code: 'uz',
            file: 'uz-UZ.js'
          },
          {
            code: 'eng',
            file: 'eng-ENG.js'
          },
        ],
        strategy: 'prefix_and_default',
        defaultLocale: 'uz',
        lazy: true,
        langDir: 'lang/',
      }

    ],
    'bootstrap-vue/nuxt',


  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {

  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  }
}
